﻿using System;
using UnityEngine;
using System.Collections;

public class ZombieMovement : MonoBehaviour
{

    //[SerializeField][Range(0f,2f)] private float movingSpeed;
    private Zombie zombie;

    private IEnumerator StartEating(Plant plant)
    {
        float startingSpeed = zombie.zombieStats.movementSpeed; // Gauname pradini greiti pries valgant.
        zombie.zombieStats.movementSpeed = 0;
        while (plant != null)
        {
            plant.plantStats.isBeingEaten = true;
            plant.TakeDamage(zombie.zombieStats.damage);
            yield return new WaitForSeconds(zombie.zombieStats.eatingRate);

        }

        zombie.zombieStats.movementSpeed = startingSpeed;

    }

    private IEnumerator MoveLeft()
    {

        while (true)
        {
        
            transform.Translate(Vector2.left * zombie.zombieStats.movementSpeed * Time.deltaTime);
            yield return null;
            
        }


    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.GetComponent<Plant>() != null)
        {
            // Atsitrenkiam i plant
            StartCoroutine(StartEating(other.gameObject.GetComponent<Plant>()));

        }


    }

    void Start() // 1 Karta ivyksta
    {
        zombie = GetComponent<Zombie>();
        StartCoroutine(MoveLeft());
    }

    



}

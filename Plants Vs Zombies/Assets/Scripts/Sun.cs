﻿using UnityEngine;
using System.Collections;

public class Sun : MonoBehaviour {

    private Player player;

	// Use this for initialization
	void Start () {
	
        player = FindObjectOfType<Player>();
        Destroy(this.gameObject, 5f);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown(){

        player.AddSuns(50);
        Destroy(this.gameObject);

    }

}

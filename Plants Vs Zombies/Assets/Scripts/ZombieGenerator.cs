﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieGenerator : MonoBehaviour {

    [SerializeField]
    private float spawnRate;

    [SerializeField]
    private GameObject zombiePrefab;

    [SerializeField]
    private int maxAmount;

    [SerializeField]
    private List<float> rowsY;

    [SerializeField]
    private float xStart;

    private int currentAmount;


    public void SpawnZombie(){

        float randomY = rowsY[Random.Range(0, rowsY.Count)];
        GameObject newZombie = (GameObject)Instantiate(zombiePrefab, new Vector2(xStart, randomY), Quaternion.identity);

       
    }

    private IEnumerator StaartSpawning(){

        while (currentAmount < maxAmount)
        {
            yield return new WaitForSeconds(spawnRate);
            SpawnZombie();
            currentAmount++;

        }

    }

    private void Start(){

        StartCoroutine(StaartSpawning());
    }
	
}

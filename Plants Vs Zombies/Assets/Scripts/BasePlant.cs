﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BasePlant : MonoBehaviour {

    public PlantStats plantStats;


    public IEnumerator RenegerateHealth(){

        while (plantStats.health < 100 && !plantStats.isBeingEaten)
        {

            yield return new WaitForSeconds(0.5f);
            plantStats.health += 1.5f;

        }


    }

    public IEnumerator TryRegenerate(){

        while (plantStats.isBeingEaten)
        {
            yield return new WaitForSeconds(2f);
            plantStats.isBeingEaten = false;
            StartCoroutine(RenegerateHealth());

        }


    }
    public void TakeDamage(float amount) {

        if (plantStats.health - amount > 0) {
            plantStats.health -= amount;
        } else {
            plantStats.health = 0;
            Destroy(this.gameObject);
        }

        StartCoroutine(TryRegenerate());


    }
}

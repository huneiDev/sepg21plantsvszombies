﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlantStats
{
    public float damage;
    public float flySpeed;
    public float fireRate;
    public float health;
    public int price;
    public bool isBeingEaten = false;

}


public class Plant : BasePlant {

    [SerializeField]
    GameObject pea;
    
 
    IEnumerator ShootPeas() {

        while (true) {

            yield return new WaitForSeconds(plantStats.fireRate);
            Shoot();
        }


    }


  
    public void Shoot() {

        GameObject newPea = (GameObject)Instantiate(pea);
        Vector2 plantPosition = transform.position;
        newPea.transform.position = plantPosition;
        newPea.GetComponent<PeaShooter>().plant = this;

    }

  

    private void Start() {

        StartCoroutine(ShootPeas());

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour {

    [SerializeField]
    private GameObject shootingDefaultPlant;

    [SerializeField]
    private GameObject peanut;

    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private Player player;

    public enum PlantType{

        Default,
        Peanut


    }

    private void SetSelectablePlant(GameObject plant){

        gameManager.currentSelectedPlant = plant;

    }

    public void Purchase(int price){

        if (player.currentSuns >= price)
        {
            player.RemoveSuns(price);
            gameManager.currentSelectedPlant = null;
        }
        else
        {
            if (gameManager.currentSelectedPlant != null)
            {
                gameManager.currentSelectedPlant = null;

            }

        }

    }

    public void PurchasePlant(string plantname){


    
        switch (plantname)
        {

            case "defaultplant":

                    if (player.currentSuns >= 100)
                    {

                    SetSelectablePlant(shootingDefaultPlant);
                        
                    }


                break;


            case "peanut":

                    if (player.currentSuns >= 50)
                    {
                    SetSelectablePlant(peanut);

                   
                    }


                break;

        }



    }





}

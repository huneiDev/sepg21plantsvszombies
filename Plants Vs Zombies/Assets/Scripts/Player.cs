﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    public Text sunText;

    public int currentSuns;

    public void AddSuns(int amount)
    {

        currentSuns += amount;
        sunText.text = currentSuns.ToString();
    }

    public void RemoveSuns(int amount)
    {
        currentSuns -= amount;
        sunText.text = currentSuns.ToString();

    }


}

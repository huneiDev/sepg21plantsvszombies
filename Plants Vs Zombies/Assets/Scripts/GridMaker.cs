﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridMaker : MonoBehaviour {

    [SerializeField]
    private GameObject tilePrefab;

    [SerializeField]
    private Vector2 startPosition;

    [SerializeField]
    Tile tile;

    [SerializeField]
    private int xRow = 9;

    [SerializeField]
    private int yRow = 5;

    [SerializeField]
    private Vector2 tileOffset;

    public int xBounds;
    public int yBounds;

    [SerializeField]
    private List<Tile> tiles = new List<Tile>();

    void Start(){

        GenerateGrid();

    }

    void CreateTile(float x, float y, int xCoord, int yCoord)
    {
        GameObject newTile = (GameObject)GameObject.Instantiate(tilePrefab, new Vector2(x,y),Quaternion.identity);
        Tile tile = newTile.GetComponent<Tile>();
        tile.x = xCoord;
        tile.y = yCoord;
        newTile.name = "Tile ("+tile.x +":" + tile.y + ")";
        tiles.Add(tile);
        

    }

    void GenerateGrid()
    {
        for (int i = 0; i < xRow; i++)
        {

            for (int b = 0; b < yRow; b++)
            {
                float newX = startPosition.x + (i * tileOffset.x);
                Debug.Log("Gautas Rez " + (i - tileOffset.x));
                float newY = startPosition.y + (b * tileOffset.y);
                CreateTile(newX, newY, i, b);

            }
        }



        }

  
    }





﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class ZombieStats
{
    [Range(0f,100f)]
    public float hp;
    [Range(0f,1f)]
    public float movementSpeed;


    public float damage;

    [Range(0, 5f)] public float eatingRate;


}

public class Zombie : MonoBehaviour
{

    public bool isSlowed = false;

    public ZombieStats zombieStats;

    private GameManager gameManager;
//
//    public IEnumerator SlowZombie(float duration)
//    {
//        
//            Debug.Log(duration);
//            float startingSpeed = zombieStats.movementSpeed;
//            zombieStats.movementSpeed /= 2;
//            isSlowed = true;
//            yield return new WaitForSeconds(0.1f);
//            Debug.Log("Praejo " + 0.1f + "Sekundes");
//            zombieStats.movementSpeed = startingSpeed;
//            isSlowed = false;
//      
//    }


    private void Start(){

        gameManager = FindObjectOfType<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D other){


        if (other.gameObject.tag == "edge")
        {
            gameManager.SetGameOver();


        }


    }


    public void TakeDamage(float amount)
    {
        if (zombieStats.hp - amount > 0)
        {
            zombieStats.hp -= amount;
        }
        else
        {
            zombieStats.hp = 0;
            Destroy(this.gameObject);
        }


    }


    
}

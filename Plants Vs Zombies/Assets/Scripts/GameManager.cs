﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {


    public GameObject currentSelectedPlant = null;

    [SerializeField]
    private Shop shop;

    [SerializeField]
    private GameObject gamecanvas;

    [SerializeField]
    private GameObject gameoverCanvas;

    void Start(){
        Time.timeScale = 1;

    }

    void Update(){

        if (Input.GetMouseButtonDown(0))
        {

            DoRay();
        }

    }


    public void SetGameOver(){

        gamecanvas.SetActive(false);
        gameoverCanvas.SetActive(true);
        Time.timeScale = 0;


    }

    public void RestartGame(){

       
        Application.LoadLevel(0);

    }

    void DoRay(){ // Tikrints, ant kur buvo paspausta pelyte.

        Camera camera = Camera.main;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);

        if (hit.transform)
        {
            // Jeigu mes paspaudem ant kazkokio gameobject.
            if (hit.transform.gameObject.GetComponent<Tile>() != null)
            {
                if (currentSelectedPlant != null)
                {
                    Tile tile = hit.transform.gameObject.GetComponent<Tile>();
                    GameObject newPlant = (GameObject)Instantiate(currentSelectedPlant, tile.transform.position, Quaternion.identity);
                    shop.Purchase(newPlant.GetComponent<BasePlant>().plantStats.price);

                }

            }
        }

    }


}

﻿using UnityEngine;
using System.Collections;

public class SunflowerGenerator : MonoBehaviour {


    [SerializeField]
    private GameObject sunPrefab;

    [SerializeField]
    float lowestTime;

    [SerializeField]
    float highestTime;

	
	void Start () {
	
        StartCoroutine(SpawnSunflowers());

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public IEnumerator SpawnSunflowers(){


        while (true)
        {
            float randomTime = Random.Range(lowestTime,highestTime);
            yield return new WaitForSeconds(randomTime);
            GameObject newSunflower = GameObject.Instantiate(sunPrefab);
            float randomX = Random.Range(-4.8f, 3.4f);
            float y = 4.35f;
            newSunflower.transform.position = new Vector2(randomX, y);

           
        }

    }

}

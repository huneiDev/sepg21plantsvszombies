﻿using UnityEngine;
using System.Collections;

public class Sunflower : BasePlant {

    [SerializeField]
    private GameObject sun;

    [SerializeField]
    private float spawnRate;

	// Use this for initialization
	void Start () {
	
        StartCoroutine(SpawnSunFlowers());

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator SpawnSunFlowers(){

        while (true)
        {
            yield return new WaitForSeconds(spawnRate);
            SpawnSunflower();

        }


    }

    public void SpawnSunflower(){

        GameObject newSun = GameObject.Instantiate(sun);
        newSun.transform.position = transform.position;
        newSun.GetComponent<Rigidbody2D>().gravityScale = 0;

    }

}

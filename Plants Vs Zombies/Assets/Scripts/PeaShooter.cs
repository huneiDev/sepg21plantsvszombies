﻿using UnityEngine;
using System.Collections;

public class PeaShooter : MonoBehaviour
{


    public BasePlant plant;


    private IEnumerator MoveRight() {

        while (true) {

            transform.Translate(Vector2.right * plant.plantStats.flySpeed * Time.deltaTime);
            yield return null;

        }

    }




    private void OnCollisionEnter2D(Collision2D other)
    {

       
        if (other.gameObject.GetComponent<ZombieMovement>() != null) // == > < != <= >=
        {

            Zombie targetZombie = other.gameObject.GetComponent<Zombie>();

//            if (!targetZombie.isSlowed) // Jeigu jis ner uzslowintas
//            {
//                StartCoroutine(targetZombie.SlowZombie(1f));

//            }

            targetZombie.TakeDamage(plant.plantStats.damage);

           Destroy(gameObject); 
        }


    }

    private void OnTriggerEnter2D(Collider2D other){


        if (other.gameObject.tag == "Garbage")
        {
            Destroy(this.gameObject);

        }

    }


    void Start()
    {

        StartCoroutine(MoveRight());

    }
}
